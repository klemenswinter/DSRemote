

set(SRC
    about_dialog.cpp
    connection.cpp
    decode_dialog.cpp
    #interface.cpp
    lan_connect_thread.cpp
    main.cpp
    mainwindow.cpp
    #mainwindow_constr.cpp
    playback_dialog.cpp
    read_settings_thread.cpp
    #save_data.cpp
    save_data_thread.cpp
    screen_thread.cpp
    #serial_decoder.cpp
    settings_dialog.cpp
    signalcurve.cpp
    tdial.cpp
    #timer_handlers.cpp
    tled.cpp
    wave_dialog.cpp
    wave_view.cpp

    edflib.c

    tmc_lan.c
    utils.c
)

if (WITH_USB_SUPPORT)
    list(APPEND SRC
        tmc_dev.c
    )
endif()

set(HDR
    about_dialog.h
    connection.h
    decode_dialog.h
    edflib.h
    global.h
    lan_connect_thread.h
    mainwindow.h
    playback_dialog.h
    read_settings_thread.h
    save_data_thread.h
    screen_thread.h
    settings_dialog.h
    signalcurve.h
    tdial.h
    tled.h

    tmc_dev.h
    tmc_lan.h
    utils.h
    wave_dialog.h
    wave_view.h
)



add_executable(dsremote ${SRC} ${HDR})

qt_add_resources(dsremote imageresources
    PREFIX "/"
    FILES
        images/r_dsremote.png
        images/playpause.png
        images/record.png
        images/stop.png
)

set_target_properties(dsremote PROPERTIES
    WIN32_EXECUTABLE ON
    MACOSX_BUNDLE ON
)
target_include_directories(dsremote
    PUBLIC
        ${CMAKE_SOURCE_DIR}
        ${CMAKE_CURRENT_BINARY_DIR}
)

target_link_libraries(dsremote
PUBLIC
    kiss_fft
    Qt6::Core
    Qt6::Widgets
    Qt6::Network
)
